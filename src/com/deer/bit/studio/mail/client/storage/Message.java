package com.deer.bit.studio.mail.client.storage;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class Message {

  private EmailAddress from;
  private EmailAddress to;
  private EmailAddress dw;
  private ZonedDateTime zonedDateTime;
  private String subject;
  private String message;
  private boolean read;
  private List<String> attachments;
  
  public Message() {
    attachments = null;
    from = null;
    to=null;
    dw=null;
    subject = "";
    message = "";
  }

  public EmailAddress getFrom() {
    return from;
  }

  public void setFrom(EmailAddress from) {
    this.from = from;
  }

  public EmailAddress getTo() {
    return to;
  }

  public void setTo(EmailAddress to) {
    this.to = to;
  }

  public EmailAddress getDw() {
    return dw;
  }

  public void setDw(EmailAddress dw) {
    this.dw = dw;
  }

  public String getZonedDateTime() {
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy MM dd");
    return zonedDateTime.format(formatter);
  }

  public void setZonedDateTime(ZonedDateTime zonedDateTime) {
    this.zonedDateTime = zonedDateTime;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public boolean isRead() {
    return read;
  }

  public void setRead(boolean read) {
    this.read = read;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }
  public void addAttachment(String a){
    if (attachments==null){
      attachments = new ArrayList<>();
    }
    attachments.add(a);
  }
  public List<String> getAttachments(){
    return attachments;
  }
}
