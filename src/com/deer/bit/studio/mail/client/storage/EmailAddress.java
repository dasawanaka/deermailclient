package com.deer.bit.studio.mail.client.storage;

/**
 *
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class EmailAddress {

  private String recipient; // eg 'michal.mateusz.daniel'  
  private String domain; // eg 'gmail.com'   

  public EmailAddress(String recipient, String domain) {
    if (isValidEmailAress(recipient, domain)) {
      this.recipient = recipient;
      this.domain = domain;
    } else {
      throw new IllegalArgumentException("E-mail adress is INVALID.");
    }

  }

  private boolean isValidEmailAress(String recipient, String domain) {
    boolean result = true;
    if (recipient != null && domain != null) {
      if (domain.contains(".")) {
        result = domain.indexOf(".") != (domain.length() - 1);
      } else {
        result = false;
      }
    } else {
      result = false;
    }
    return result;
  }

  public String asString() {
    if(recipient == null || domain == null){
      return null;
    }
    return recipient + "@" + domain;
  }
};
