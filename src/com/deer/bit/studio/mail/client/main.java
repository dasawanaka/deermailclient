package com.deer.bit.studio.mail.client;

import com.deer.bit.studio.mail.client.view.splash.screen.SplashScreen;
import com.deer.bit.studio.mail.client.view.userInterface.start.LoginForm;
import com.deer.bit.studio.mail.client.view.userInterface.app.Application;

/**
 *
 * @author Michał Daniel (michal.daniel@makolab.com)
 */
public class main {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws InterruptedException {

    SplashScreen splashScreen = new SplashScreen();
    splashScreen.setVisible(true);
    for (int i = 0; i < 100; i++) {
      Thread.sleep(5);
      splashScreen.setProgressValue(i);
    }
    Application mailBox = new Application();
    mailBox.setVisible(false);
    LoginForm loginForm = new LoginForm(mailBox);
    splashScreen.setVisible(false);
    loginForm.setVisible(true);
  
    
  }

}
