package com.deer.bit.studio.mail.client.vocabulary;

import java.awt.Color;

/**
 * @author Michał Daniel (michal.mateusz.daniel@gmail.com)
 */
public class GuiColor {

  //exit button
  public static final Color EXIT_PRESSED_COLOR = new Color(200, 20, 20);
  public static final Color EXIT_BASIC_COLOR = new Color(255, 0, 0);
  public static final Color EXIT_RELAESED_COLOR = new Color(255, 0, 0);

  //log in button
  public static final Color LOGIN_PRESSED_COLOR = new Color(22, 129, 6);
  public static final Color LOGIN_BASIC_COLOR = new Color(10, 220, 10);
  public static final Color LOGIN_RELAESED_COLOR = new Color(10, 220, 10);

  //left side color
  public static final Color LEFT_SIDE_COLOR = new Color(0, 102, 204);

  //button
  public static final Color BUTTON_PRESSED_COLOR = new Color(11, 73, 204);
  public static final Color BUTTON_SELECTED_COLOR = new Color(11, 73, 204);
  public static final Color BUTTON_BASIC_COLOR = new Color(0, 20, 225);
  public static final Color BUTTON_RELAESED_COLOR = new Color(0, 20, 225);
  
  //list
  public static final Color LIST_SELECT_COLOR = new Color(51, 153, 255, 100);
  public static final Color LIST_BASIC_COLOR = new Color(255,255,255);
  
  //scrollbar
  public static final Color SCROLLBAR_DRAGING_COLOR = new Color(0, 51, 77);
  public static final Color SCROLLBAR_THUMB_ROLLOVER_COLOR = new Color(0, 136, 204);
  public static final Color SCROLLBAR_BASIC_COLOR = new Color(0, 136, 204, 100);
  
}
